#  https://gitlab.com/jan.hosek.ddm/hry
import sys, pygame
from random import randint, choice
pygame.init()
SIZE = WIDTH, HEIGHT = (900, 200)
BACKGROUND = (200, 200, 200)
black = (0, 0, 0)

screen = pygame.display.set_mode(SIZE)
clock = pygame.time.Clock()

class Dino(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surfs = []
        self.index = 0
        for i in range(1, 4):
            self.surfs.append(pygame.image.load("dino%s.png" % i).convert_alpha())
        self.surf = self.surfs[0]
        self.rect = self.surf.get_rect()
        self.rect.y = 115
        self.rect.x = 40
        sel.skok = False
    
    def update(self, klavesy):
        self.index += 1
        self.surf = self.surfs[(self.index//3)%len(self.surfs)]
        if klavesy[pygame.K_SPACE] and not self.skok:
            self.skok = True
        
        if self.skok:
            pass
            # TODO
        

class Kaktus(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf = pygame.image.load("kaktus.png").convert_alpha()
        self.rect = self.surf.get_rect()
        self.rect.y = 115
        self.rect.x = 900
        self.rychlost = [-5, 0]
    
    def update(self):
        self.rect.move_ip(self.rychlost)
        if self.rect.left < 0:
            self.kill()


dino = Dino()

vsechny_postavy = pygame.sprite.Group()
prekazky = pygame.sprite.Group()

vsechny_postavy.add(dino)

konec = False

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    if konec:
        continue
    klavesy = pygame.key.get_pressed()
    dino.update(klavesy)
    prekazky.update()
    screen.fill(BACKGROUND)
    
    if randint(0, 1000) < 10:
        k = Kaktus()
        prekazky.add(k)
        vsechny_postavy.add(k)
    
    
    for postava in vsechny_postavy:
        screen.blit(postava.surf, postava.rect)
    
    pygame.draw.line(screen, black, [0, 170], [1000, 170], 2)
    pygame.display.flip()
    
    if pygame.sprite.spritecollideany(dino, prekazky):
        konec = True

    clock.tick(40)